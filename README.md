# Prerequisites

[node.js](https://nodejs.org/)

[yt-dlp](https://github.com/yt-dlp/yt-dlp)

[ffmpeg](https://ffmpeg.org/)

# SponsorBlock info

[ffmpeg](https://ffmpeg.org/) is required. When the SponsorBlock option is selected, all categories will be removed from the video.

# Run

```js

node .\yt-dlp33.js

```

# Compile

Compile using [pkg](https://www.npmjs.com/package/pkg)

```js

pkg  yt-dlp33.js  --target  node16

```
