const readline = require('readline');
const childProcess = require('child_process');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const showMenu = () => {
  console.log('\nChoose an option:');
  console.log('1. yt-dlp');
  console.log('2. yt-dlp audio');
  console.log('3. yt-dlp with SponsorBlock');
  console.log('4. yt-dlp audio with SponsorBlock');
  console.log('5. Exit');
}

const runCommand = (command) => {
  try {
    childProcess.execSync(command, { stdio: 'inherit' });
    console.log('\nDone!');
  } catch (error) {
    console.error(error);
  }
}

const main = () => {
  showMenu();

  rl.question('\nEnter an option: ', option => {
    if (option === '1') {
      rl.question('Enter a URL: ', url => {
        rl.question('Enter output directory path: (press enter for current dir)', directoryPath => {
          let command;
          if (directoryPath === '') {
            command = `yt-dlp ${url} --downloader aria2c --output "${process.cwd()}/%(title)s.%(ext)s"`;
          } else {
            command = `yt-dlp ${url} --downloader aria2c --output "${directoryPath}/%(title)s.%(ext)s"`;
          }
          runCommand(command);
          main();
        });
        
      });
    } else if (option === '2') {
      rl.question('Enter a URL: ', url => {
        rl.question('Enter output directory path: (press enter for current dir): ', directoryPath => {
          let command;
          if (directoryPath === '') {
            command = `yt-dlp ${url} --extract-audio --downloader aria2c --output "${process.cwd()}/%(title)s.%(ext)s"`;
          } else {
            command = `yt-dlp ${url} --extract-audio --downloader aria2c --output "${directoryPath}/%(title)s.%(ext)s"`;
          }
          
          runCommand(command);
          main();
        });
        
      });
    } else if (option === '3') {
      rl.question('Enter a URL: ', url => {
        rl.question('Enter output directory path: (press enter for current dir): ', directoryPath => {
          let command;
          if (directoryPath === '') {
            command = `yt-dlp ${url} --sponsorblock-remove all --downloader aria2c --output "${process.cwd()}/%(title)s.%(ext)s"`;
          } else {
            command = `yt-dlp ${url} --sponsorblock-remove all --downloader aria2c --output "${directoryPath}/%(title)s.%(ext)s"`;
          }
          
          runCommand(command);
          main();
        });
        
      });
    } else if (option === '4') {
      rl.question('Enter a URL: ', url => {
        rl.question('Enter output directory path: (press enter for current dir): ', directoryPath => {
          let command;
          if (directoryPath === '') {
            command = `yt-dlp ${url} --extract-audio --sponsorblock-remove all --downloader aria2c --output "${process.cwd()}/%(title)s.%(ext)s"`;
          } else {
            command = `yt-dlp ${url} --extract-audio --sponsorblock-remove all --downloader aria2c --output "${directoryPath}/%(title)s.%(ext)s"`;
          }
          
          runCommand(command);
          main();
        });
        
      });
    } else if (option === '5') {
      console.log('\nExiting...');
      rl.close();
    } else {
      console.log('\nInvalid option');
      main();
    }
  });
}

main();